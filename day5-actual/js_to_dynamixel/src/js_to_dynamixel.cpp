// Copyright 2020 ROBOTIS CO., LTD.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// 2021/07/23
// Suzuki modified to communicate with AX-12A through sensor_msgs/JointState.

/*************************************************************************************S
 * This node communicate with DYNAMIXEL AX-12A through U2D2.
 * This node subscribes to /joint_state (sensor_msgs/JointState) to set motor angle
 * and publishes actual motor angle to /present_joint_state (sensor_msgs/JointState).
*************************************************************************************/

#include <ros/ros.h>

#include "sensor_msgs/JointState.h"
#include "dynamixel_sdk/dynamixel_sdk.h"

using namespace dynamixel;

// Control table address: https://emanual.robotis.com/docs/en/dxl/ax/ax-12a/
#define ADDR_TORQUE_ENABLE    24
#define ADDR_GOAL_POSITION    30
#define ADDR_PRESENT_POSITION 36

// Protocol version
#define PROTOCOL_VERSION      1.0             // Latest protcol version 2.0 doesn't support AX-12A

// Serial setting
#define BAUDRATE              57600
#define DEVICE_NAME           "/dev/ttyUSB0"  // [Linux] To find assigned port, use "$ ls /dev/ttyUSB*" command

// Node's frequency
#define NODE_FREQUENCY        10              // Hz

PortHandler * portHandler;
PacketHandler * packetHandler;

void setPositionCallback(const sensor_msgs::JointState::ConstPtr &msg)
{
  uint8_t dxl_error = 0;
  int dxl_comm_result = COMM_TX_FAIL;
  uint16_t position = 0;
  const double motor_direction[5] = {1.,1.,-1.,-1.,1.};

  for (int i = 0; i < 5; ++i)
  {
    position = (uint16_t)fmax(fmin(((motor_direction[i]*msg->position[i]/((5./6.)*M_PI)+1.)*512.),1023.),0.);
    dxl_comm_result = packetHandler->write2ByteTxRx(
      portHandler, (uint8_t)(i+1), ADDR_GOAL_POSITION, position, &dxl_error);
    if (dxl_comm_result == COMM_SUCCESS) {
      ROS_INFO("setPosition : [ID:%d] [POSITION:%d]", i+1, position);
    } else {
      ROS_ERROR("Failed to set position! Result: %d", dxl_comm_result);
    }
  }
}

int main(int argc, char ** argv)
{
  uint8_t dxl_error = 0;
  int dxl_comm_result = COMM_TX_FAIL;
  int dxl_addparam_result = false;
  sensor_msgs::JointState present_js;
  int16_t position_raw = 0;

  portHandler = PortHandler::getPortHandler(DEVICE_NAME);
  packetHandler = PacketHandler::getPacketHandler(PROTOCOL_VERSION);

  if (!portHandler->openPort()) {
    ROS_ERROR("Failed to open the port!");
    return -1;
  }

  if (!portHandler->setBaudRate(BAUDRATE)) {
    ROS_ERROR("Failed to set the baudrate!");
    return -1;
  }

  for (int i = 1; i <= 5; ++i)
  {
    dxl_comm_result = packetHandler->write1ByteTxRx(
      portHandler, i, ADDR_TORQUE_ENABLE, 1, &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS) {
      ROS_ERROR("Failed to enable torque for Dynamixel ID %d", i);
      return -1;
    }
  }

  ros::init(argc, argv, "read_write_node");
  ros::NodeHandle nh;
  ros::Subscriber set_position_sub = nh.subscribe("/joint_states", 10, setPositionCallback);
  ros::Publisher get_position_pub = nh.advertise<sensor_msgs::JointState>("/present_joint_states", 1);
  ros::Rate cycle_rate(NODE_FREQUENCY);

  present_js.name.resize(5);
  present_js.position.resize(5);
  for (int i = 0; i < 5; ++i)
  {
    present_js.name[i] = "joint" + std::to_string(i+1);
    present_js.position[i] = 0;
  }

  while(ros::ok())
  {
    ros::spinOnce();

    dxl_error = 0;
    dxl_comm_result = COMM_TX_FAIL;

    for (int i = 0; i < 5; ++i)
    {
      dxl_comm_result = packetHandler->read2ByteTxRx(
        portHandler, (uint8_t)(i+1), ADDR_PRESENT_POSITION, (uint16_t *)&position_raw, &dxl_error);
      if (dxl_comm_result == COMM_SUCCESS)
      {
        ROS_INFO("getPosition : [ID:%d] -> [POSITION:%d]", i+1, position_raw);
        present_js.position[i] = ((double)position_raw/512.-1.)*((5./6.)*M_PI);
      }
      else
      {
        ROS_INFO("Failed to get position! Result: %d", dxl_comm_result);
      }
    }
    present_js.header.stamp = ros::Time::now();
    get_position_pub.publish(present_js);

    cycle_rate.sleep();
  }

  portHandler->closePort();
  return 0;
}
