# day5-actual

## Preparences
Dynamixel SDKをインストールします．
~~~
$ sudo apt install ros-noetic-dynamixel-sdk
~~~
今回使うコードをリポジトリからダウンロードして，ワークスペースにコピーします．
~~~
$ cd ~/tomato2021
$ git pull
$ cp -r ~/tomato2021/day5-actual ~/catkin_ws/src
~~~
ファイルを開いて`~/catkin_ws/src`に`day5-actual`が追加されていることを確認してください．
また，使用するDynamixelモータのボーレートがわかっていない場合は，Dynamixel Workbenchもダウンロードします．
~~~
$ cd ~/catkin_ws/src/day5-actual
$ git clone https://github.com/ROBOTIS-GIT/dynamixel-workbench.git
$ git clone https://github.com/ROBOTIS-GIT/dynamixel-workbench-msgs.git
~~~

## Configure and Build
CRANE+と通信を行うにあたって，いくつか設定を行う必要があります．
以下のコマンドにより，シリアルポートへのアクセスルールを変更します．
~~~
$ cd ~/catkin_ws/src/day5-actual
$ wget https://raw.githubusercontent.com/ROBOTIS-GIT/dynamixel-workbench/master/99-dynamixel-workbench-cdc.rules
$ sudo cp ./99-dynamixel-workbench-cdc.rules /etc/udev/rules.d/
$ sudo udevadm control --reload-rules
$ sudo udevadm trigger
~~~
CRANE+をPCにUSB接続し，以下のコマンドでどのポートに接続されたか確認します．
~~~
$ ls /dev/ttyUSB*
~~~

ボーレートがわからない場合は，Dynamixel Workbenchを使って調べる必要があります．
~~~
$ cd ~/catkin_ws
$ catkin build
$ roscore
~~~
CRANE+のモータ電源を入れます．
別のターミナルを開いて，以下のコマンドを入力．ただし，`/dev/ttyUSB0`の部分は
先程ポートを確認したときに表示された番号に直す必要があるので注意．
~~~
$ rosrun dynamixel_workbench_controllers find_dynamixel /dev/ttyUSB0
~~~
順にスキャンが行われ，あるボーレートで５つのモータが見つかるはずです．
スキャンが完了したらモータ電源は切っておきましょう。

今回使うノードを開いて，ポートとボーレートの部分を変更します．
~~~
$ gedit ~/catkin_ws/src/day5-actual/js_to_dynamixel/src/js_to_dynamixel.cpp
~~~
`#define`で定義されている`BAUDRATE`と`DEVICE_NAME`の値を，先程調べた値に変更してください．
変更したら，再びビルドします．
~~~
$ cd ~/catkin_ws
$ catkin build js_to_dynamixel
~~~
以上でセットアップ完了です．

ポート名はUSBを抜き差しするたびに変わる可能性があるので，毎回調べる必要があります．
また，別のCRANE+を使う場合は，ボーレートを確認しておく必要がります．

## Run CRANE+
day3の動きを実機に適用します．
day3のプログラムを実行するとCRANE+が急に動き出すので注意してください．

roscoreを立ち上げます．
~~~
$ roscore
~~~
CRANE+をPCにUSB接続し，モータ電源を入れます．
別のターミナルを開いて，以下のコマンドを実行．
~~~
$ rosrun js_to_dynamixel js_to_dynamixel
~~~
最後に，また別のターミナルを開いて，day3で使ったシミュレーションを立ち上げます．
このコマンドを実行するとCRANE+が急に動き出すので注意してください．
~~~
$ roslaunch day3-crane_ik arm4d_ik.launch
~~~

## Run Kobuki
day4の動きを実機に適用します．
KobukiをPCにUSB接続し，Kobukiの電源を入れます．
以下のコマンドを実行．
~~~
$ roslaunch turtlebot_bringup minimal.launch
~~~
KobukiがPCに接続され，"ピロリロリ〜"と音がなります．
別のターミナルを開いて以下のコマンドを実行．
~~~
$ rosrun kobuki_operation kobuki_key_operation
~~~
Kobukiをキーボードで操作できるようになります．

`kobuki_key_operation`ノードを`Ctrl+C`で終了し，自動操縦のノードを立ち上げます．
~~~
$ rosrun kobuki_operation kobuki_custom_operation
~~~
Kobukiが設定したとおりに動けば成功です．
