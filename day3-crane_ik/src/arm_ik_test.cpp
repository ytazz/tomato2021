#include <ros/ros.h>
#include "arm_ik.h"

int main(int argc, char **argv) {
  float link1 = 0.0454;
  float link2 = 0.026;
  float link3 = 0.083;
  float link4 = 0.0935;
  float link5 = 0.1;
  ArmMock arm_mock(link1, link2, link3, link4, link5);
  ArmSolver arm_solver(link1, link2, link3, link4, link5);

  geometry_msgs::Point target_point;
  target_point.x = 0.3;
  target_point.y = 0.0;
  target_point.z = 0.2;
  ROS_INFO("input pos: %f, %f, %f", target_point.x, target_point.y, target_point.z);
    
  Angle4D angles;
  if(arm_solver.solve(target_point, 1.5707, angles)){
    arm_mock.setAngle(angles);
    ROS_INFO("angles: %f, %f, %f, %f, %f", angles.angle1, angles.angle2, angles.angle3, angles.angle4, angles.angle5);
    geometry_msgs::Point output_point = arm_mock.getTargetPoint();
    ROS_INFO("input pos: %f, %f, %f", output_point.x, output_point.y, output_point.z);
  }
  else{
    ROS_INFO("can not solve");
  }
   
  return 0;
}
