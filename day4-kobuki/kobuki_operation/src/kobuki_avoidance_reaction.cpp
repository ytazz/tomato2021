#include <kobuki_operation/kobuki_operation.hpp>


void KobukiOperation::avoidanceReaction()
{
    if (_bumper_which == kobuki_msgs::BumperEvent::RIGHT)
    {
        kobukiStop();
        kobukiKeep(1);
    }
    else if (_bumper_which == kobuki_msgs::BumperEvent::CENTER)
    {
        kobukiStop();
        kobukiKeep(1);
        kobukiMove(-0.5, 0);
        kobukiKeep(1);
        kobukiStop();
        kobukiKeep(1);
    }
    else if (_bumper_which == kobuki_msgs::BumperEvent::LEFT)
    {
        kobukiStop();
        kobukiKeep(1);
    }
}
