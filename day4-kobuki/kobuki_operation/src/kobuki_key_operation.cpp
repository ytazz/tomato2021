#include <kobuki_operation/kobuki_operation.hpp>


void KobukiOperation::normalOperation()
{
    ros::spinOnce();

    char key = getchar();
    if (key != EOF)
    {
        if (key == '\x03')
        {
            _exit_program = true;
            return;
        }
        char key_n;
        while ((key_n = getchar()) != EOF)
        {
            key = key_n;
            if (key == '\x03')
            {
                _exit_program = true;
                return;
            }
        }

        switch (key)
        {
        case 'q': _speed*=1.1; _turn*=1.1; printVels(); break;
        case 'z': _speed*=0.9; _turn*=0.9; printVels(); break;
        case 'w': _speed*=1.1; _turn*=1.0; printVels(); break;
        case 'x': _speed*=0.9; _turn*=1.0; printVels(); break;
        case 'e': _speed*=1.0; _turn*=1.1; printVels(); break;
        case 'c': _speed*=1.0; _turn*=0.9; printVels(); break;

        case 'i': kobukiMove( _speed,      0); _stop_count=0; break;
        case 'o': kobukiMove( _speed, -_turn); _stop_count=0; break;
        case 'j': kobukiMove(      0,  _turn); _stop_count=0; break;
        case 'l': kobukiMove(      0, -_turn); _stop_count=0; break;
        case 'u': kobukiMove( _speed,  _turn); _stop_count=0; break;
        case ',': kobukiMove(-_speed,      0); _stop_count=0; break;
        case '.': kobukiMove(-_speed,  _turn); _stop_count=0; break;
        case 'm': kobukiMove(-_speed, -_turn); _stop_count=0; break;

        case ' ': case 'k': kobukiStop(); break;
        
        default:
            break;
        }
    }
    else
    {
        if (++_stop_count > 0.4*_freq) kobukiMove(0, 0);
    }
    kobukiInterpolate();
    _update_rate.sleep();
}