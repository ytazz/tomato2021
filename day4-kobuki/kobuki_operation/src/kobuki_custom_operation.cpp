#include <kobuki_operation/kobuki_operation.hpp>
#define kobukiKeep(t) kobukiKeep(t,false)


void KobukiOperation::normalOperation()
{
    kobukiMove(0.5,0);
    kobukiKeep(3);
    kobukiStop();
    kobukiKeep(2);
    kobukiMove(-0.5,0);
    kobukiKeep(3);
    kobukiStop();
    kobukiKeep(2);
}