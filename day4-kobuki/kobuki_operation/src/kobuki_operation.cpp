#include <kobuki_operation/kobuki_operation.hpp>


KobukiOperation::KobukiOperation(double freq) :
    _nh(),
    _freq(freq),
    _update_rate(freq),
    _prev_bumper_time(),
    _bumper_pressed(false),
    _bumper_which(0),
    _control{0},
    _speed(0.5),
    _turn(1.0),
    _speed_acc(1.0),
    _turn_acc(5.0),
    _print_status(0),
    _stop_count(0),
    _exit_program(false)
{
    _bumper_sub = _nh.subscribe<kobuki_msgs::BumperEvent>(
        "/mobile_base/events/bumper",
        1,
        &KobukiOperation::bumperCallback,
        this
    );

    _kobuki_pub = _nh.advertise<geometry_msgs::Twist>(
        "/mobile_base/commands/velocity",
        1
    );
}


void KobukiOperation::printHowTo()
{
    printf(
        "Control Your Turtlebot!\n"
        "---------------------------\n"
        "Moving around:\n"
        "   u    i    o\n"
        "   j    k    l\n"
        "   m    ,    .\n"
        "\n"
        "q/z : increase/decrease max speeds by 10%%\n"
        "w/x : increase/decrease only linear speed by 10%%\n"
        "e/c : increase/decrease only angular speed by 10%%\n"
        "space key, k : force stop\n"
        "anything else : stop smoothly\n"
        "\n"
        "CTRL-C to quit\n"
    );
}


void KobukiOperation::printVels()
{
    printf(
        "currently:\tspeed %.3f\tturn %.3f\n",
        _speed, _turn
    );
    if (++_print_status > 14)
    {
        printHowTo();
        _print_status = 0;
    }
}


void KobukiOperation::spin()
{
    ///< awaken timer
    ros::Duration(0.1).sleep();

    ///< change terminal setting
    struct termios old_terminal, new_terminal;
    int old_fcntl;
    tcgetattr(STDIN_FILENO, &old_terminal);
    new_terminal = old_terminal;
    new_terminal.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &new_terminal);
    old_fcntl = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, old_fcntl | O_NONBLOCK);

    printHowTo();
    printVels();
    while (ros::ok() && !_exit_program)
    {
        if (_bumper_pressed)
        {
            _bumper_pressed = false;
            avoidanceReaction();
        }
        else
        {
            normalOperation();
        }
    }
    kobukiStop();

    ///< restore terminal setting
    tcsetattr(STDIN_FILENO, TCSANOW, &old_terminal);
    fcntl(STDIN_FILENO, F_SETFL, old_fcntl);
}


void KobukiOperation::bumperCallback(const kobuki_msgs::BumperEventConstPtr &event)
{
    ros::Time now = ros::Time::now();
    double delta = (now - _prev_bumper_time).toSec();
    _prev_bumper_time = now;
    if (delta < 3 && event->bumper == _bumper_which) return;
    if (event->state == kobuki_msgs::BumperEvent::PRESSED)
    {
        _bumper_pressed = true;
        _bumper_which = event->bumper;
    }
}


void KobukiOperation::kobukiMove(double speed, double turn)
{
    if (_exit_program) return;
    _control.target_speed = speed;
    _control.target_turn = turn;
    kobukiInterpolate();
}


void KobukiOperation::kobukiKeep(double duration, bool exit_w_interrupt)
{
    if (duration <= 0) return;
    ros::Time start_time = ros::Time::now();
    char key;
    while(ros::ok() && !_exit_program && (ros::Time::now() - start_time).toSec() < duration)
    {
        ros::spinOnce();
        if (getchar() == '\x03')
        {
            _exit_program = true;
            break;
        }
        if (_bumper_pressed)
        {
            if (exit_w_interrupt) return;
            _bumper_pressed = false;
            struct control_t old_c = _control;
            ros::Time start_avoidance = ros::Time::now();
            avoidanceReaction();
            start_time += ros::Time::now() - start_avoidance;
            _control = old_c;
        }
        else
        {
            kobukiInterpolate();
            _update_rate.sleep();
        }
    }
}


void KobukiOperation::kobukiStop()
{
    _control = {0};
    kobukiInterpolate();
}


void KobukiOperation::kobukiInterpolate()
{
    if (_control.target_speed > _control.control_speed)
        _control.control_speed = fmin(_control.target_speed, _control.control_speed + _speed_acc/_freq);
    else if (_control.target_speed < _control.control_speed)
        _control.control_speed = fmax(_control.target_speed, _control.control_speed - _speed_acc/_freq);
    else
        _control.control_speed = _control.target_speed;

    if (_control.target_turn > _control.control_turn)
        _control.control_turn = fmin(_control.target_turn, _control.control_turn + _turn_acc/_freq);
    else if (_control.target_turn < _control.control_turn)
        _control.control_turn = fmax(_control.target_turn, _control.control_turn - _turn_acc/_freq);
    else
        _control.control_turn = _control.target_turn;
    
    geometry_msgs::Twist command;
    command.linear.x = _control.control_speed;
    command.angular.z = _control.control_turn;
    _kobuki_pub.publish(command);
}