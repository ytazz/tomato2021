
# day2-communication

第二回② 指令の送受信で用いるパッケージです。

## how to build
```bash
$ cd ~  # go to HOME directory or, if you want, SOME directory.
$ git clone https://gitlab.com/ytazz/tomato2021.git
$ cp -r tomato2021/day2-* ~/catkin_ws/src/
$ cd ~/catkin_ws && catkin build
```

## how to use

### ひとつひとつNode起動
```bash
$ roscore
```
```bash
$ rosrun day2-communication talker
```
```bash
$ rosrun day2-communication listener
```

### roslaunch
```bash
$ roslaunch day2-communication communication.launch
```
