
# day2-crane
第二回③ ロボットアームの表示で用いるパッケージです。

## how to use

### Joint State PublisherのGUIなし
```bash
$ roslaunch day2-crane crane.launch
```

### Joint State PublisherのGUIあり
```bash
$ roslaunch day2-crane crane.launch gui:=true
```
