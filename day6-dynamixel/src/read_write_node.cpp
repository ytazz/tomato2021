// Copyright 2020 ROBOTIS CO., LTD.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*******************************************************************************
 * This example is written for DYNAMIXEL X(excluding XL-320) and MX(2.0) series with U2D2.
 * For other series, please refer to the product eManual and modify the Control Table addresses and other definitions.
 * To test this example, please follow the commands below.
 *
 * Open terminal #1
 * $ roscore
 *
 * Open terminal #2
 * $ rosrun dynamixel_sdk_examples read_write_node
 *
 * Open terminal #3 (run one of below commands at a time)
 * $ rostopic pub -1 /set_position dynamixel_sdk_examples/SetPosition "{id: 1, position: 0}"
 * $ rostopic pub -1 /set_position dynamixel_sdk_examples/SetPosition "{id: 1, position: 1000}"
 * $ rosservice call /get_position "id: 1"
 * $ rostopic pub -1 /set_position dynamixel_sdk_examples/SetPosition "{id: 2, position: 0}"
 * $ rostopic pub -1 /set_position dynamixel_sdk_examples/SetPosition "{id: 2, position: 1000}"
 * $ rosservice call /get_position "id: 2"
 *
 * Author: Zerom
*******************************************************************************/

#include <ros/ros.h>

#include "std_msgs/String.h"
#include "dynamixel_sdk/dynamixel_sdk.h"

using namespace dynamixel;

// Control table address
#define ADDR_OPERATING_MODE   11
#define ADDR_TORQUE_ENABLE    64
#define ADDR_GOAL_CURRENT     102
#define ADDR_GOAL_VELOCITY    104
#define ADDR_GOAL_POSITION    116
#define ADDR_PRESENT_POSITION 132

// Protocol version
#define PROTOCOL_VERSION      2.0             // Default Protocol version of DYNAMIXEL X series.

// Default setting
#define DXL1_ID               10               // DXL1 ID
#define BAUDRATE              1000000
//#define BAUDRATE              57600           // Default Baudrate of DYNAMIXEL X series
#define DEVICE_NAME           "/dev/ttyUSB0"  // [Linux] To find assigned port, use "$ ls /dev/ttyUSB*" command

#define CURRENT_MODE          0
#define VELOCITY_MODE         1
#define POSITION_MODE         3

#define NODE_FREQUENCY        200             // Hz


PortHandler * portHandler;
PacketHandler * packetHandler;

int main(int argc, char ** argv)
{
  uint8_t dxl_error = 0;
  int dxl_comm_result = COMM_TX_FAIL;
  uint16_t position_read = 0;
  uint16_t position_write = 0;
  int16_t dir = 1;

  portHandler = PortHandler::getPortHandler(DEVICE_NAME);
  packetHandler = PacketHandler::getPacketHandler(PROTOCOL_VERSION);

  if (!portHandler->openPort()) {
    ROS_ERROR("Failed to open the port!");
    return -1;
  }

  if (!portHandler->setBaudRate(BAUDRATE)) {
    ROS_ERROR("Failed to set the baudrate!");
    return -1;
  }

  //// change operating mode. current/velocity/position mode
  //dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, ADDR_OPERATING_MODE, CURRENT_MODE, &dxl_error);
  //dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, ADDR_OPERATING_MODE, VELOCITY_MODE, &dxl_error);
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, ADDR_OPERATING_MODE, POSITION_MODE, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS) {
    ROS_ERROR("Failed to change mode for Dynamixel ID %d", DXL1_ID);
    return -1;
  }else{
    ROS_INFO("Success to change mode for Dynamixel ID %d", DXL1_ID); 
  }



  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, ADDR_TORQUE_ENABLE, 1, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS) {
    ROS_ERROR("Failed to enable torque for Dynamixel ID %d", DXL1_ID);
    return -1;
  }else{
    ROS_INFO("Success to enable torque for Dynamixel ID %d", DXL1_ID); 
  }

  ros::init(argc, argv, "read_write_node");
  ros::NodeHandle nh;
  ros::Rate cycle_rate(NODE_FREQUENCY);

  while(ros::ok())
  {
    ros::spinOnce();

    dxl_error = 0;
    dxl_comm_result = COMM_TX_FAIL;

    /* current mode
    dxl_comm_result = packetHandler->write2ByteTxRx(portHandler, DXL1_ID, ADDR_GOAL_CURRENT, position_write, &dxl_error);
    if (dxl_comm_result == COMM_SUCCESS) {
      ROS_INFO("setCurrent : [ID:%d] [CURRENT:%d]", 1, position_write);
    } else {
      ROS_INFO("Failed to set current! Result: %d", dxl_comm_result);
    }
    */

    /* velocity mode
    dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, DXL1_ID, ADDR_GOAL_VELOCITY, position_write, &dxl_error);
    if (dxl_comm_result == COMM_SUCCESS) {
      ROS_INFO("setVelocity : [ID:%d] [VELOCITY:%d]", 1, position_write);
    } else {
      ROS_INFO("Failed to set velocity! Result: %d", dxl_comm_result);
    }
    */

    ///* position mode
    dxl_comm_result = packetHandler->write4ByteTxRx(portHandler, DXL1_ID, ADDR_GOAL_POSITION, position_write, &dxl_error);
    if (dxl_comm_result == COMM_SUCCESS) {
      ROS_INFO("setPosition : [ID:%d] [POSITION:%d]", 1, position_write);
    } else {
      ROS_INFO("Failed to set position! Result: %d", dxl_comm_result);
    }
    //*/
   
    dxl_comm_result = packetHandler->read2ByteTxRx(portHandler, DXL1_ID, ADDR_PRESENT_POSITION, (uint16_t *)&position_read, &dxl_error);
    if (dxl_comm_result == COMM_SUCCESS)
    {
      ROS_INFO("getPosition : [ID:%d] -> [POSITION:%d]", 1, position_read);
    } else {
      ROS_INFO("Failed to get position! Result: %d", dxl_comm_result);
    }

    position_write = position_write + dir;
    if (position_write > 2000){
      dir = -1;
    }else if (position_write  < 100) {
      dir = 1;
    }
    cycle_rate.sleep();
    
  }
  
  dxl_comm_result = packetHandler->write1ByteTxRx(portHandler, DXL1_ID, ADDR_TORQUE_ENABLE, 0, &dxl_error);
  if (dxl_comm_result != COMM_SUCCESS) {
    ROS_ERROR("Failed to disable torque for Dynamixel ID %d", DXL1_ID);
    return -1;
  }else{
    ROS_INFO("Success to disable torque for Dynamixel ID %d", DXL1_ID); 
  }


  portHandler->closePort();

  return 0;
}
