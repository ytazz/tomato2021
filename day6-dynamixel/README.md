
# day6-dynamixel

第6回 dynamixelサーボモータを動作させる際に用いるパッケージです。

## how to build
```bash
$ cd ~  # go to HOME directory or, if you want, SOME directory.
$ git clone https://gitlab.com/ytazz/tomato2021.git
$ cp -r tomato2021/day6-* ~/catkin_ws/src/
$ catkin build
```

## how to use

```bash
$ roscore
```
```bash
$ rosrun day6-dynamixel read_write_node
```


